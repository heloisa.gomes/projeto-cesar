# Projeto César

Este projeto, que meu querido colega César tão generosamente possibilitou ao Squad, é uma linda oportunidade para treinar e reforçar o conhecimento sobre Linux. 

O projeto consiste em:

1     - Instalar um segundo servidor Oracle Linux na ultima versao disponivel

2     - Ajustar o servidor na rede BRIGDE com IP fixo

3     - Criar um filesystem de 10GB /kubernetes

4     - Alterar a REDE do servidor1 (criado na atividade anterior) para BRIDGE e configurar um IP FIXO

5     - Criar relação de confiança entre os dois servidores

6     - criar um novo disco no servidor1 com 5GB e exportar ele por NFS para o servidor2 

7     - Configurar um cluster Kubernetes com 1 master e 1 worder.

8     - Subir um pod XXX no kubernetes -> RedisMYSQL

9     - Tornar o pod acessivel externamente e evidenciar o funcionamento da aplicação

10    - criar um unico script q valide o serviço do kubernetes para garantir que esteja ativo e rodando nos 2 servidores. O script deve rodar a cada 10 minutos e salvar a saida no NFS
 
 10.1 - gerar a seguinte saida: "HOSTNAME1 - kubernetes no ar - dataHORA" / "Hostname2 - Kubernetes no ar - dataHORA"
 
 10.2 - se o serviço estiver fora, deve gerar a seguinte saida: "HOSTNAME1 - Subindo serviço" / "HOstname2 - Subindo serviço" e reiniciar o serviço do kubernetes

11    - Versionar e documentar todo o processo

Obrigações:

1 - Obrigatorio acesso via PUTTY ou WSL

Restrições:

1 - Proibido acesso à console após ajustar o IP Fixo;

2 - Proibido logar como root

3 - Proibido digitar senha para logar

4 - Proibido logar nos servidores utilizando IP

5 - Proibido usar CMD para acesso SSH

Links recomendados para apoio:

Redes:

https://www.youtube.com/watch?v=V6HT5lXo4g0
https://www.redhat.com/sysadmin/sysadmin-essentials-networking-basics

Linux Basic:

https://www.actualtechmedia.com/wp-content/uploads/2017/12/CUMULUS-NETWORKS-Linux101.pdf
https://www.youtube.com/watch?v=dvvP4wpsAQI&list=PLKCk3OyNwIzvZR0-OoTtScm2NNo68gWtq&index=2


Toda a história do projeto pode ser acompanhada através dos [Issue Boards do GitLab](https://gitlab.com/heloisa.gomes/projeto-cesar/-/boards).

O arquivo `README.md` é o primeiro arquivo a ser lido quando o projeto é acessado. Toda a documentação está na pasta `docs`.

## Instalação

Os tutoriais de instalação e configuração do projeto estão na [pasta docs](docs). Abaixo será listada a sequência que deve ser seguida para realizar a instalação do projeto.

### Instalar e configurar a VM

- [Instalar e configurar máquina virtual local](docs/install_oracle.md)

### Instalando o Putty

- [Instalar o Putty no seu computador](docs/install_putty.md)

### Criar o filesystem

- [Criar o filesystem](docs/create_filesystem.md)
	
### Estabelecer relação de confiança entre os servidores

- [Estabelecer a relação de confiança](docs/ssh_key.md)

### Criar novo disco e exportar para o outro servidor

- [Criar novo disco no servidor 1](docs/create_disk.md)
- [Exportar o disco para o servidor 2](docs/export_disk.md)

### Instalação do Docker

- [Instalação do Docker](docs/install_docker.md)

### Instalação do Kubernetes

- [Instalação do Kubernetes](docs/install_k8s_kubeadm.md)

### Instalação do MySQL

- [Instalação do MySQL no Kubernetes](docs/install_mysql.md)


## Autores

* **Heloisa Gomes** - [heloisa.gomes](https://gitlab.com/heloisa.gomes)

