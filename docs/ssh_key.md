# Estabelecendo a relação de confiança 

Este documento apresenta uma forma de estabelecer a relação de confiança entre os dois servidores criados.

### Criar chave RSA

Para estabelecer uma relação de confiança entre os dois servidore, será necessário também uma chave SSH. Então iremos criar ela agora no servidor 1:

		ssh-keygen -t rsa -f ~/.ssh/id_rsa

O código acima cria uma chave utilizando o algoritmo de criptografia `rsa`.

Agora temos que copiar a chave pública no servidor 2. Digite o comando:

		ssh-copy-id user@ip

Este comando irá adicionar a sua chave publica ao arquivo de chaves autorizadas da máquina virtual.

Para testar o funcionamento do SSH digite:

		ssh user@ip

Se não solicitar a senha é sinal que funcionou. Para obter a relação de confiança no outro servidor basta repetir esse mesmo processo só que na outra máquina.

## Referências

http://devblog.drall.com.br/estabelecer-relacao-de-confianca-entre-maquinas-via-ssh
