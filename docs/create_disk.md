# Criar um novo disco

Agora vamos criar um novo disco de 5GB no servidor 1.

## Criando o disco

Para se criar um novo disco abra a interface do VirtualBox, abra as configurações referentes ao servidor 1, e clique na aba de 'Armazenamento':

<img src="assets/VirtualBox.PNG" alt="config_install" width="700"/>
<img src="assets/armazenamento.PNG" alt="config_install" width="700"/>

Então clique em 'Adiciona Disco Rígido':

<img src="assets/AdicionarDisco.PNG" alt="config_install" width="700"/>

Clique em 'Criar' e configure o disco da seguinte forma e adicione ele a sua VM:

<img src="assets/disco1.PNG" alt="config_install" width="700"/>
<img src="assets/disco2.PNG" alt="config_install" width="700"/>
<img src="assets/disco3.PNG" alt="config_install" width="700"/>

