# Instalação do docker

Este documento apresenta duas formas de instalar o Docker:
- Instalação manual: uma instalação onde você deve ter acesso direto ao terminal da máquina; 
- Instalação pelo ansible playbook.

## Instalação manual

Para instalar o Docker no Oracle Linux, primeiramente temos que ter instalado na máquina os utilitários do yum:

		sudo yum install yum-utils

Após a instalação dos módulos, iremos adicionar os repositórios do Docker:

		sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

Agora é só instalar o docker:

		sudo yum install -y docker-ce docker-ce-cli containerd.io

Após a instalação habilite e inicie o Docker:

		sudo systemctl enable docker
		sudo systemctl start docker

Adicione o usuário no grupo do Docker e depois reinicie a sessão:

		sudo usermod -a -G docker oracle-user

Para conferir a instalação digite:

		groups
		systemctl status docker


## Instalação pelo Ansible

[Certifique-se do Ansible estar devidamente instalado e configurado](config_ansible.md)

O projeto também possui um ansible playbook para instalar o docker.

Para utilizá-lo, entre na pasta `src/ansible` e digite o comando:

		ansible-playbook install_docker.yml -i hosts

Caso o usuário tenha alguma senha, você pode acrescentar a flag `--ask-become-pass` ao comando. Então digite a senha do usuário logado na máquina e pronto.

Aguarde a instalação concluir.


## Referências

https://docs.docker.com/engine/install/centos/

