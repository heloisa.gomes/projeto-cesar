# Instalação do SO Oracle Linux 8.6

O projeto utilizará o sistema operacional Oracle Linux 8.6. Antes de iniciar a instalação, é necessário que tenhamos instalado no computador o software de virtualização de máquinas [VirtualBox](https://www.virtualbox.org/wiki/Downloads).

Após a instalação do VirtualBox, iremos proceder à instalação do Oracle Linux 8.5.

## Download

Faça o download da ISO no site da Oracle: [https://yum.oracle.com/oracle-linux-isos.html](https://yum.oracle.com/oracle-linux-isos.html). 

Este projeto utilizou a versão 8.6.

## Configurações iniciais

Após instalar o VirtualBox e baixar a imagem da distribuição Oracle Linux, iremos criar a máquina virtual. Então abra seu VirtualBox e vá em `Novo`.

![virtualbox1](assets/Captura de tela de 2022-02-25 11-21-21.png)

Dê um nome para a máquina, selecione o tipo **Linux** e coloque a versão como **Oracle**.

Na próxima etapa iremos definir o tamanho da memória RAM. Para essa máquina serão dedicados 4GB.

Avançando as etapas, chegamos na parte de criação do disco rígido virtual. Utilizamos 15GB.

Após definir tudo clique no botão `criar`. E após criar a máquina iremos iniciá-la, clicando no botão `Iniciar`.

Na primeira inicialização, o VirtualBox pedirá para acrescentar a imagem do sistema operacional. Você deve acrescentar a imagem ISO do Oracle Linux.


## Instalação do sistema em ambiente virtual

Ao iniciar a máquina virtual com a imagem do sistema carregada, iremos visualizar a interface gráfica de instalação do Anaconda.

Nesta interface você deve configurar as partições do disco, definir uma senha para o usuário root e criar um novo usuário.

No presente projeto utilizamos todo o espaço do disco para a pasta raiz. Criamos também um usuário com o nome **oracle**

<img src="assets/Captura de tela de 2022-02-22 17-00-31.png" alt="config_install" width="700"/>

<img src="assets/Captura de tela de 2022-02-22 17-00-07.png" alt="config_install" width="700"/>

<img src="assets/Captura de tela de 2022-02-22 17-11-09.png" alt="config_install" width="700"/>


Após realizar as configurações inicie a instalação do sistema. 

<img src="assets/Captura de tela de 2022-02-22 17-11-35.png" alt="installing..." width="700"/>

Depois de algum tempo o sistema será instalado com sucesso e a máquina irá reiniciar.



## Pós instalação

### Atualizações e dependencias

Após instalar o sistema iremos realizar algumas configurações para preparar o ambiente

Primeiro iremos atualizar os pacotes do sistema:

		sudo yum update -y

Após tudo atualizado iremos instalar o python3, que será utilizado pelo **ansible**.

		sudo yum install python3 -y

### Redes

Por padrão, o IP da máquina é dinâmico. Então ao reiniciar a máquina ou o serviço de rede ocorre uma mudança no IP. Para não ter que ficar descobrindo o IP da máquina toda hora, iremos configurar um IP estático.

Na linha de comando digite

		sudo nmtui

Irá aparece uma interface com algumas opções. Vá em `Editar uma conexão` e depois selecione sua interface de rede.

Aqui você fará as configurações necessárias de acordo com a sua rede.

<img src="assets/configuração-de-rede.PNG" alt="" width="700"/>


Neste exemplo modificamos a configuração IPv4 para manual.

Em endereços foi adicionado o endereço IP padrão da máquina (192.168.15.110).

E em DNS foram adicionados o IP do gateway da rede e o IP de servidor (192.168.15.1) de DNS do Google (8.8.8.8).

Após salvar as configurações reinicie o serviço de rede.

		sudo systemctl restart NetworkManager


### Nome da máquina

Também daremos um nome à máquina, para identificá-la facilmente na rede.

		sudo hostnamectl set-hostname master

Após mudar o nome da  máquina, adicione o nome e o IP da mesma no arquivo hosts da sua máquina local e da sua máquina virtual (/etc/hosts). Ex.:

		192.168.15.110       master


## Conclusão

Após a instalação e configuração do Oracle Linux 8.6, podemos prosseguir com o desenvolvimento do projeto.


## Referencias

https://docs.oracle.com/en/operating-systems/oracle-linux/8/install/

