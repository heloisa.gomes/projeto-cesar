# Instalação do MySQL

O MySQL é instalado no kubernetes através de arquivos yaml.

Os *yaml files* possuem o namespace `projetocompass`, o arquivo de geração de senha do MySQL e o deployment do MySQL. O deployment possui o serviço e o persistent volume.

## Instalação pelo Ansible

Para instalar o MySQL utilizando o Ansible, basta entrar na pasta `src/ansible` e executar o comando abaixo:

		ansible-playbook -i hosts install_mysql.yml

O playbook irá copiar os arquivos yaml para a máquina master e irá executá-los através da ferramenta kubectl.


## Instalação manual

Caso queira instalar manualmente o MySQL, basta entrar na pasta `src/ansible/roles/mysql/files` e copiar os arquivos para a máquina virtual.

Após copiar os arquivos, você deve criar uma pasta `/data/mysql` na sua máquina virtual (ou no minikube). Essa pasta será o **volume do MySQL**.

Depois de criar essa pasta, altere o usuário e o grupo da pasta do mysql:

		sudo chown -R 1000:1000 /data/mysql

Agora vá até a pasta onde estão os arquivos `yaml` e aplique os arquivos do MySQL:

		kubectl apply -f namespace-proj-compass.yaml
		kubectl apply -f mysql-pass.yaml
		kubectl apply -f mysql-deployment.yaml
	
Para verificar se está tudo ok, você pode executar o comando abaixo:

		kubectl get all -n projetocompass

## Próximos passos

Agora [crie o pipeline no Jenkins](./create_pipeline.md) para instalar o WordPress.

## Referências

https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/