# Criando filesystem

Este documento apresenta os comandos utilizados para criar um filesystem no servidor 2:

## Comandos

Para se criar um novo filesystem, primeiramente temos que criar uma partição do disco no qual vamos criar:

		sudo fdisk /dev/sdb

Após a partição, iremos criar o filesystem do tipo ext4:

		sudo mkfs.ext4  /dev/sdb1

Agora temos que realizar o mount no sistema operacional, então primeiramente vamos procurar o UUID do filesystem criado (/dev/sdb1) e salvar o valor encontrado:

		sudo blkid

Então vamos criar o diretório em que iremos fazer o mount:

		sudo mkdir /kubernetes

E realizaremos o mount:

		sudo mount -t ext4 /dev/sdb1  /kubernetes/

Para conferir se foi realizado com sucesso:

		sudo df -h

Fazendo o mount pela linha de comando acaba não o tornando persistente, então agora vamos adicionar uma linha contendo o UUID do filesytem ao arquivo /etc/fstab:

		UUID=$UUID /kubernetes/ ext4  defaults   0 0

Após alterar o arquivo /etc/fstab podemos realizar o unmount e verificar se as configurações se mantiveram as mesmas:

		sudo umount /kubernetes/
		sudo mount -a
		sudo df -h

## Referências

https://opensource.com/article/19/4/create-filesystem-linux-partition

https://www.linuxnaweb.com/como-utilizar-o-lvm-no-linux/

https://web.mit.edu/rhel-doc/3/rhel-sag-pt_br-3/s1-filesystem-ext3-create.html
