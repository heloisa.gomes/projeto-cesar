# Instalação do Jenkins no kubernetes

Existem duas opções para instalar o Jenkins no kubernetes:
- Instalar manualmente
- Instalar através de um ansible playbook

## Instalação manual

Primeiramente é necessário criar o namespace do jenkins:

		kubectl create namespace jenkins

Depois de criar o namespace, aplique o arquivo de deployment:

		kubectl apply -f jenkins-deployment.yaml

E pra finalizar, aplique o arquivo de serviço, para que você possa acessar o jenkins externamente:

		kubectl apply -f jenkins-service.yaml

Pegue a porta do serviço (por padrão é a 30010):

		kubectl get svc -n jenkins

E abra o jenkins no navegador:

		http://<IP DO NODE>:<PORTA DO SERVICO DO JENKINS>

Agora siga para as [configurações iniciais do Jenkins](#configura%C3%A7%C3%B5es-iniciais-do-jenkins).


## Instalação através do ansible

Para instalar o Jenkins no kubernetes utilizando o ansible, basta entrar na pasta `src/ansible` e executar o comando:

		ansible-playbook -i hosts install_jenkins.yml

Para verificar a instalação do jenkins:

		kubectl get all -n jenkins

Acesse o IP do node com a porta do jenkins e faça as primeiras configurações do jenkins, como instalar plugins e configurar as informações do usuário.


## Configurações iniciais do Jenkins

Após abrir o Jenkins no navegador, você verá uma tela assim:

<img src="assets/Captura de tela de 2022-02-25 13-22-30.png" alt="" width="700"/>

Para pegar essa senha inicial é necessário acessar o pod do jenkins.

		kubectl get pod -n jenkins

		kubectl exec -it 

Dentro do pod do jenkins, digite:

		cat /var/jenkins_home/secrets/initialAdminPassword

* Atenção: O caminho pode ser diferente do exemplo!

A saída do comando vai ser a senha inicial do Jenkins. Copie essa senha e coloque no *input*. 

Depois será necessário instalar alguns plugins.

Depois de instalar os plugins você já está pronto para criar o primeiro pipeline.


## Referências

https://www.jenkins.io/doc/book/installing/kubernetes/