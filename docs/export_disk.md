# Exportando o disco

Nessa página esta descrito como exportar o disco criado no servidor 1 para o servidor 2 por NFS.

## Preparando o disco

### Particionando o disco

Para podermos exportar esse disco precisamos particiona-lo primeiro:

		sudo fdisk /dev/sdb
		sudo fdisk -l

O segundo comando lhe permite verificar se deu certo o particionamento.

### Criando Volume Físico

Agora vamos criar um volume físico para o disco.

		sudo pvcreate /dev/sdb1
		sudo pvdisplay

Novamente o segundo comando é para conferir se o volume físico foi criado.

### Criando Volume Group

Então temos que colocar esse volume físico dentro de um grupo de volumes.

		sudo vgcreate nfs-vg /dev/sdb1

Utilizamos o seguinte comando para habilitar o Volume Group

		sudo vgchange -a y

E para se certificar de que está correto:

		sudo vgscan
		sudo vgdisplay -v nfs-vg

### Criando Volume Lógico

Temos criar um volume lógico para definir um sistema de arquivos para armazenar os dados.

		sudo lvcreate -L 5GB -n nfs-vg nfs-lv
		sudo lvdisplay

### Criando o sistema de arquivos EXT4

Para finalizar vamos criar um sistema de arquivos:

		sudo mkfs.ext4 /dev/nfs-vg/nfs-lv

E realizar o mount:

		mkdir /nfs-share
		sudo mount /dev/nfs-vg/nfs-lv /nfs-share/

Para conferir se foi realizado com sucesso:

		sudo df -h

Fazendo o mount pela linha de comando acaba não o tornando persistente, então vamos pegar o UUID e adicionar uma linha contendo o os dados ao arquivo /etc/fstab:

		sudo blkid
		sudo vi /etc/fstab
		UUID=$UUID /nfs-share/ ext4  defaults   0 0

Após alterar o arquivo /etc/fstab podemos realizar o unmount e verificar se as configurações se mantiveram as mesmas:

		sudo umount /nfs-share/
		sudo mount -a
		sudo df -h

## Configurando o Export

### Preparando o NFS

Agora com o disco pronto vamos relizar a configuração do nfs para exportar o disco:

		sudo yum install nfs-utils

Com o pacote instalado, vamos identificar a pasta como uma NFS export adicionando uma linha ao arquivo /etc/exports:

		sudo vi /etc/exports
		/nfs-share $IP(rw)

Subistituindo o $IP pelo ip do servidor 2. Para que ele posso acessar e modificar os arquivos do diretório adicionamos o parametro (rw).

### Habilitando o Firewall

Temos que indicar ao firewall o nfs como um serviço: 

		firewallcmd --permanent --zone=public --add-service=nfs
		firewallcmd --reload
		firewallcmd --list-all

### Iniciando o serviço NFS

Porfim agora vamos iniciar o serviço:

		systemctl start nfs-server
		systemctl enable nfs-server

Podemos conferir os exports com o comando:

		showmount -e

## Acessando o export no Servidor 2 

### Preparando o NFS

Primeiro vamos ter que instalar o NFS no servidor 2 também:

		sudo yum install nfs-utils

Temos que criar uma pasta para relizar o mount do NFS:

		sudo mkdir /nfs-mount

Agora usamos o seguinte comando para o mount:

		sudo mount $IP:/nfs-share /nfs-mount

Subistituindo o '$IP' pelo ip do servidor 1, então agora todos os arquivos que adicionados na pasta /nfs-share do servidor 1 vão aparecer na /nfs-mount do servidor 2.

Então só precisamos adicionar uma linha ao final do arquivo contendo o os dados do mount /etc/fstab:

		sudo vi /etc/fstab
		$IP:/nfs-share  /nfs-mount   nfs   rw   0 0

Então só mudar o '$IP' pelo ip do servidor 1 e salvar o arquivo e utilizar o seguinte comando para automaticamente realizar o mount:

		sudo mount -a

## Referencias

https://www.linuxnaweb.com/como-utilizar-o-lvm-no-linux/

https://www.youtube.com/watch?v=fnVoVzB8Px0

https://www.youtube.com/watch?v=YFeaOEgFrto
