# Instalalação do kubernetes utilizando o Kubeadm

Esse documento apresenta a instalação do kubernetes utilizando o **Kubeadm**.

## Instalando o Kubeadm

Para instalar o kubeadm, siga os passos abaixo:

Copie o script `install_k8s_kubeadm.sh` localizado em `src/ansible/roles/kubernetes/files/` para todas as suas máquinas virtuais e execute-o:

		sh install_k8s_kubeadm.sh

Após a instalação do kubernetes, você pode prosseguir para a etapa de [configuração dos nodes](#configura%C3%A7%C3%A3o-do-kubeadm).


## O que o script faz?

O script `install_k8s_kubeadm.sh` faz uma série de modificações no sistema preparando-o para a instalação do kubernetes. Depois de preparar o sistema, as ferramentas **kubeadm** e **kubectl** serão instaladas, junto com o serviço **kubelet**.

Então o script executará as seguintes tarefas:
- Desabilitar o SELinux, para permitir que os containers acessem o sistema de arquivos do sistema, permitindo que os pods e os serviços de rede funcionem corretamente.

- Desabilitar o Firewall.

- Adicionar o repositório do kubernetes.

- Mudar o cgroup do docker.

- Instalar o kubernetes.

- Habilitar o kubelet.

Após tudo isso o kubernetes vai estar pronto para ser configurado.

## Configuração do Kubeadm

Após a instalação do kubernetes, você deve configurar os nodes (master e workers).

### Node master

Para configurar a máquina master, execute os comandos abaixo:		

		sudo kubeadm init --pod-network-cidr=10.244.0.0/16

No final do processo, você deve obter o token do master, que será utilizado pelos workers para se conectar ao master.

Ainda no master execute os comandos abaixo para configurar a ferramenta **kubectl**:

		mkdir -p $HOME/.kube
		sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
		sudo chown $(id -u):$(id -g) $HOME/.kube/config

Ao executar o comando `kubectl get nodes`, você verá que o master já foi criado.

Agora configure os worker nodes.

### Node worker

Para configurar os workers, execute os comandos abaixo, trocando o `token` e o `ip` para os que estão configurados no master:

		sudo kubeadm join --token <token> <ip>:6443

O comando vai ficar semelhante ao exemplo abaixo:

		sudo kubeadm join 172.31.78.7:6443 --token zji0zo.25hdm3way6gijsez --discovery-token-ca-cert-hash sha256:829ca70e0d0477b516e2e04ac3cea075ec59f3d833d448a53153b8e12eb57110


Ao executar o comando `kubectl get nodes`, você verá o master e os workers que já foram criados.

Agora configure a rede do kubernetes.
