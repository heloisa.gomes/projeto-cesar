# Configurando o Putty

Essa documentação se refere a instalação do Putty na sua maquina local para conectar com a VM.

## Instalando o Putty

Fazer o download do arquivo de instalção do Putty e instalar:

Download - https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html

## Conectar com a VM

Ao abrir o aplicativo do Putty, basta digitar o IP da sua VM, na port 22 para acesso via SSH

<img src="assets/putty.PNG" alt="config_install" width="700"/>

Depois é só logar com seu usuário e senha.

## Referências

https://www.youtube.com/watch?v=lGsjkofS9eQ

