#!/bin/bash

# Instala GIT
sudo yum install git -y

# Desabilita o SELinux para permitir que os containers acessem o sistema de arquivos do sistema
# Isso é necessário para que os serviços de rede dos pods funcionem corretamente
sudo setenforce 0
sudo sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/sysconfig/selinux

# Desabilitar o firewall (se existir)
sudo systemctl disable firewalld
sudo modprobe br-netfilter
echo 'net.bridge.bridge-nf-call-iptables = 1' | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

# Adicionar o repositório do Kubernetes
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# Reiniciar docker
sudo systemctl daemon-reload
sudo systemctl restart docker

# Instalar o iproute-tc
sudo yum install -y iproute-tc

# Instalar o kubeadm
sudo yum install kubeadm -y

# Desativar o SWAP
# O kubelet não funciona com o swap ativado
sudo swapoff -a
sudo sed -i '/ swap / s/^/#/' /etc/fstab

# Habilitar o serviço do kubelet
sudo systemctl enable kubelet
sudo systemctl start kubelet

# Entre na máquina do kubernetes master e inicie o kubeadm:
# sudo kubeadm init --pod-network-cidr=10.244.0.0/16

# Após configurar o master entre na rede do cluster e execute o comando, manualmente:
#	kubeadm join 192.168.201.9:6443 --token i8gowt.xwke5bnuqn3jah0g \
#	--discovery-token-ca-cert-hash sha256:ca0842e57b703e161c2de0ceddbaaef50d3ac2663bbd8ecebc4fde2b6f6f5617

