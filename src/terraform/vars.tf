variable "ami_oracle_8" {
  type = map(any)

  default = {
    us-east-1 = "ami-0b0af3577fe5e3532"
  }
}

variable "cdirs_acesso_ssh" {
  type = list(any)

  default = ["0.0.0.0/0"]
}

variable "cdirs_acesso_http" {
  type = list(any)

  default = ["0.0.0.0/0"]
}

variable "cdirs_acesso_internet" {
  type = list(any)

  default = ["0.0.0.0/0"]
}

variable "key_name" {
  default = "terraform-aws"
}
