# Definicao dos provedores
terraform {
  required_providers {
    aws = {
      source  = "aws"
      version = "4.1"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

# Definir as instancias
# Node master
resource "aws_instance" "master" {
  ami           = var.ami_oracle_8["us-east-1"]
  instance_type = "t3.small"
  key_name      = var.key_name # chave de acesso para ssh
  tags = {
    Name = "master-01"
  }

  private_ip = "172.31.11.201"

  security_groups = ["default"]

  vpc_security_group_ids = [
    "${aws_security_group.acesso-ssh.id}"
  ]
}

# worker
resource "aws_instance" "worker" {
  ami           = var.ami_oracle_8["us-east-1"]
  instance_type = "t2.micro"
  key_name      = var.key_name
  tags = {
    Name = "worker-01"
  }

  private_ip = "172.31.88.103"

  security_groups = ["default"]

  vpc_security_group_ids = [
    "${aws_security_group.acesso-ssh.id}",
    "${aws_security_group.acesso-http.id}",
    "${aws_security_group.acesso-jenkins.id}"
  ]
}
